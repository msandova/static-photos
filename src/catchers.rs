use rocket::catch;
use rocket::Request;
use rocket_dyn_templates::Template;
use serde_json::json;

#[catch(401)]
pub fn unauthorized() -> Template {
    let msg = "The request requires user authentication.";

    Template::render("message", json!({ "message": &msg }))
}

#[catch(404)]
pub fn not_found(req: &Request) -> Template {
    let msg = format!("Sorry, {} does not exist.", req.uri());

    Template::render("message", json!({ "message": &msg }))
}
