use rocket::data::Capped;
use rocket::form::Form;
use rocket::fs::{NamedFile, TempFile};
use rocket::http::RawStr;
use rocket::request::FromParam;
use rocket::response::Redirect;
use rocket::Config;
use rocket::{catchers, get, launch, post, routes, FromForm};

use rocket_auth::User;
use rocket_auth::Users;
use rocket_dyn_templates::Template;

use serde_json::json;
use std::borrow::Cow;

// mod api_v1;
mod auth;
mod catchers;
mod utils;

static DOMAIN: &str = "http://localhost:8000";
static UPLOADS_PATH: &str = "uploads";

#[get("/")]
fn index(user: Option<User>) -> Template {
    Template::render("index", json!({ "user": user }))
}

#[get("/upload_succ/<url>")]
fn upload_succ(url: &str, user: Option<User>) -> Template {
    let url = RawStr::new(url)
        .url_decode()
        .unwrap_or_else(|_| Cow::from(""));

    Template::render("upload_succ", json!({ "url": url, "user": user }))
}

fn show_message(msg: &str) -> Redirect {
    let uri = rocket::uri!(message(msg));

    Redirect::to(uri)
}

#[get("/message/<msg>")]
fn message(msg: &str, user: Option<User>) -> Template {
    let msg = RawStr::new(msg)
        .url_decode()
        .unwrap_or_else(|_| Cow::from(""));

    Template::render("message", json!({ "message": msg, "user": user }))
}

async fn open_sqlite() -> anyhow::Result<Users> {
    let users = Users::open_sqlite("users.db").await?;
    users.create_table().await?;

    Ok(users)
}

#[launch]
async fn rocket() -> _ {
    let users = open_sqlite().await.unwrap();

    rocket::build()
        .mount(
            "/",
            routes![
                message,
                upload_succ,
                index,
                auth::get_signup,
                auth::post_signup,
                auth::get_login,
                auth::post_login,
                auth::logout,
                auth::delete,
                // api_v1::upload,
                // api_v1::replace,
                get_upload,
                post_upload,
                retrieve,
            ],
        )
        .register("/", catchers![catchers::not_found, catchers::unauthorized])
        .manage(users)
        .attach(Template::fairing())
}

#[get("/upload")]
fn get_upload(user: User) -> Template {
    Template::render("upload", json!({ "user": user }))
}

#[derive(FromForm)]
struct Upload<'r> {
    #[field(validate = utils::validate_field())]
    project: &'r str,
    #[field(validate = utils::validate_field())]
    version: &'r str,
    #[field(validate = utils::validate_field())]
    basename: &'r str,
    file: Capped<TempFile<'r>>,
}

#[post("/upload", data = "<form>")]
async fn post_upload(mut form: Form<Upload<'_>>, _user: User) -> std::io::Result<Redirect> {
    let project = &form.project;
    let version = &form.version;
    let basename = &form.basename;

    let directory = format!("{project}/{version}");

    let store_dir = format!("{UPLOADS_PATH}/{directory}");
    let store_path = format!("{store_dir}/{basename}");

    let url = format!("{DOMAIN}/{directory}/{basename}");
    let uri = rocket::uri!(upload_succ(&url));

    if !form.file.is_complete() {
        let my_config = Config::default();
        let limit = my_config.limits.get("file/png").unwrap().to_string();
        return Ok(show_message(&format!("File exceeded {} limit", limit)));
    }

    if std::path::Path::new(&store_path).exists() {
        return Ok(show_message("Screenshot already exists"));
    }

    if utils::is_valid_png(&mut form.file).await.is_ok() {
        std::fs::create_dir_all(store_dir)?;
        form.file.move_copy_to(store_path).await?;

        Ok(Redirect::to(uri))
    } else {
        Ok(show_message("Not a valid PNG"))
    }
}

#[get("/<project>/<version>/<basename>")]
async fn retrieve(project: Str<'_>, version: Str<'_>, basename: Str<'_>) -> Option<NamedFile> {
    let filename = format!(
        "{UPLOADS_PATH}/{}/{}/{}",
        project.inner, version.inner, basename.inner
    );
    NamedFile::open(&filename).await.ok()
}

#[derive(FromForm)]
struct Str<'a> {
    inner: Cow<'a, str>,
}

impl<'a> FromParam<'a> for Str<'a> {
    type Error = &'a str;

    fn from_param(param: &'a str) -> Result<Self, Self::Error> {
        match param
            .chars()
            .all(|c| c.is_ascii_alphanumeric() || c.is_ascii_punctuation())
        {
            true => Ok(Str {
                inner: param.into(),
            }),
            false => Err(param),
        }
    }
}
