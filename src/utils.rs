use rocket::fs::TempFile;

pub(crate) fn validate_field<'v>(field: &str) -> rocket::form::Result<'v, ()> {
    let err = rocket::form::Error::validation("Invalid character");

    match field
        .chars()
        .all(|c| c.is_ascii_alphanumeric() || c.is_ascii_punctuation())
    {
        true => Ok(()),
        false => Err(err.into()),
    }
}

pub(crate) async fn is_valid_png(file: &mut TempFile<'_>) -> anyhow::Result<()> {
    use std::env::temp_dir;

    if Some(&rocket::http::ContentType::PNG) != file.content_type() {
        anyhow::bail!("Not a PNG");
    }

    let path = if let Some(path) = file.path() {
        std::path::PathBuf::from(path)
    } else {
        let path = temp_dir().join(format!("{}.png", uuid::Uuid::new_v4()));
        file.persist_to(&path).await?;

        path
    };

    let decoder = png::Decoder::new(std::fs::File::open(path)?);
    decoder.read_info()?;

    Ok(())
}
