use rocket::fs::TempFile;
use rocket::serde::json::Json;
use rocket::serde::Serialize;

use crate::utils;
use crate::{DOMAIN, UPLOADS_PATH};

#[derive(Serialize)]
pub struct UploadRes {
    url: Option<String>,
    error: Option<String>,
}

impl UploadRes {
    fn err(reason: &str) -> Self {
        Self {
            url: None,
            error: Some(reason.to_string()),
        }
    }

    fn url(url: &str) -> Self {
        Self {
            url: Some(url.to_string()),
            error: None,
        }
    }
}

#[post("/api/v1/upload/<project>/<version>/<basename>", data = "<file>")]
pub(crate) async fn upload(
    project: &str,
    version: &str,
    basename: &str,
    mut file: rocket::form::Form<TempFile<'_>>,
) -> std::io::Result<Json<UploadRes>> {
    let directory = format!("{project}/{version}");

    let store_dir = format!("{UPLOADS_PATH}/{directory}");
    let store_path = format!("{store_dir}/{basename}");

    let url = format!("{DOMAIN}/{directory}/{basename}");

    if std::path::Path::new(&store_path).exists() {
        return Ok(Json(UploadRes::err("Screenshot already uploaded")));
    }

    if utils::is_valid_png(&mut file).await.is_ok() {
        std::fs::create_dir_all(store_dir)?;
        file.move_copy_to(store_path).await?;

        Ok(Json(UploadRes::url(&url)))
    } else {
        Ok(Json(UploadRes::err("Not a PNG")))
    }
}

#[post("/api/v1/replace/<project>/<version>/<basename>", data = "<file>")]
pub(crate) async fn replace(
    project: &str,
    version: &str,
    basename: &str,
    mut file: rocket::form::Form<TempFile<'_>>,
) -> std::io::Result<Json<UploadRes>> {
    let directory = format!("{project}/{version}");

    let store_dir = format!("{UPLOADS_PATH}/{directory}");
    let store_path = format!("{store_dir}/{basename}");

    let url = format!("{DOMAIN}/{directory}/{basename}");

    if !std::path::Path::new(&store_path).exists() {
        return Ok(Json(UploadRes::err("Screenshot does not exist")));
    }

    if utils::is_valid_png(&mut file).await.is_ok() {
        file.move_copy_to(store_path).await?;

        Ok(Json(UploadRes::url(&url)))
    } else {
        Ok(Json(UploadRes::err("Not a PNG")))
    }
}
