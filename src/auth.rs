use serde_json::json;

use rocket::form::Form;
use rocket::response::Redirect;
use rocket::{get, post};
use rocket_auth::User;

use rocket_auth::{Auth, Error, Login, Signup};
use rocket_dyn_templates::Template;

// logs in for a day.
static LOGING_DURATION: u64 = 60 * 60 * 24;

#[get("/login")]
pub(crate) fn get_login(user: Option<User>) -> Template {
    Template::render("login", json!({ "user": user }))
}

#[post("/login", data = "<form>")]
pub(crate) async fn post_login(form: Form<Login>, auth: Auth<'_>) -> Result<Redirect, Error> {
    // logs in for a day.
    let duration = std::time::Duration::from_secs(LOGING_DURATION);
    auth.login_for(&form, duration).await?;

    Ok(Redirect::to("/"))
}

#[get("/logout")]
pub(crate) fn logout(auth: Auth<'_>) -> Result<Template, Error> {
    auth.logout()?;

    Ok(Template::render("logout", json!({})))
}

#[get("/delete")]
pub(crate) async fn delete(auth: Auth<'_>) -> Result<Template, Error> {
    auth.delete().await?;

    Ok(Template::render("deleted", json!({})))
}

#[get("/signup")]
pub(crate) async fn get_signup(user: Option<User>) -> Template {
    Template::render("signup", json!({ "user": user }))
}

#[post("/signup", data = "<form>")]
pub(crate) async fn post_signup(form: Form<Signup>, auth: Auth<'_>) -> Result<Redirect, Error> {
    auth.signup(&form).await?;
    auth.login(&form.into()).await?;

    Ok(Redirect::to("/"))
}
