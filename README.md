# Static Photos

To run:
```sh
touch users.db
cargo run
```

Configuration can be found at `Rocket.toml`, see [docs](https://rocket.rs/v0.5-rc/guide/configuration/).
